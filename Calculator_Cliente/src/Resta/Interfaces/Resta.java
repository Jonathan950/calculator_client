package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Resta extends Remote {
    public int resta(int a, int b) throws RemoteException;

}
