package InterfacesImp;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import Interfaces.Division;

public class ClienteImp {
    public void ConnectorServer() {
        try {
            int resultado;
            Registry registry = LocateRegistry.getRegistry("127.0.0.1", 9090);
            Division division = (Division) registry.lookup("Division");
            resultado = division.division(100, 20);
            System.out.println(resultado);
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }
}
